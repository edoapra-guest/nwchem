#!/bin/bash

export RUNTESTS=`pwd`/QA/runtests.mpi.unix
export NWCHEM_TOP=`pwd`
export NWCHEM_TARGET=LINUX$(dpkg-architecture -qDEB_HOST_ARCH_BITS)
export NWCHEM_EXECUTABLE=/usr/bin/nwchem
export NWCHEM_BASIS_LIBRARY=/usr/share/nwchem/libraries/
export NWCHEM_NWPW_LIBRARY=/usr/share/nwchem/libraryps/

(cd QA; ../debian/testsuite)
